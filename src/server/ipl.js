const { matches_arr, deliveries_arr } = require('./index');
const fs = require('fs');



function economical_bowler_2015() {
    let obj = {};
    matches_arr.map(match => {
        if (match['season'] === '2015') {
            deliveries_arr.map(delivery => {
                if (delivery['match_id'] === match['id']) {
                    if (obj[delivery['bowler']] === undefined) {
                        obj[delivery['bowler']] = { 'no_of_ball': 0, 'runs_given': 0 }
                    }

                    if (delivery['wide_runs'] && delivery['noball_runs'])
                        obj[delivery['bowler']]['no_of_ball'] += 1;
                    obj[delivery['bowler']]['runs_given'] += parseInt(delivery['total_runs']);

                }

            })
        }
    })


    let economy_arr = [];

    Object.keys(obj).map(ele => {

        economy_arr.push({ 'bowler': ele, 'economy_rate': Math.round((obj[ele]['runs_given'] / (obj[ele]['no_of_ball'] / 6) * 100)) / 100 })
    })

    let top_10_economy_bowler = economy_arr.sort(function (a, b) {
        return a['economy_rate'] - b['economy_rate'];
    }).slice(0, 10);

    const addr = "../public/output/top_10_economy_bowler.json";

    write_file(addr, JSON.stringify(top_10_economy_bowler));

}

function extra_runs() {
    let extra_runs_obj = {};
    matches_arr.map(match => {
        if (match['season'] === '2016') {
            deliveries_arr.map(delivery => {
                if (delivery['match_id'] === match['id']) {
                    if (extra_runs_obj[delivery['batting_team']] === undefined) {
                        extra_runs_obj[delivery['batting_team']] = 0;
                    }

                    extra_runs_obj[delivery['batting_team']] += parseInt(delivery['extra_runs']);


                }
            })
        }
    })

    const addr = "../public/output/extra_runs.json";
    write_file(addr, JSON.stringify(extra_runs_obj));

}


function match_played_per_year() {
    let match_per_year = {};
    matches_arr.map(match => {

        if (match_per_year[match['season']] === undefined) {
            match_per_year[match['season']] = 0;
        }

        match_per_year[match['season']] += 1;

    });
    const data = JSON.stringify(match_per_year);
    const addr = '../public/output/match_per_year.json';
    write_file(addr, data);
}

function match_won_per_team_per_year() {
    let matches_won_per_team_obj = {};


    matches_arr.map(match => {
        if (match['winner'] != '' && matches_won_per_team_obj[match['winner']] === undefined) {
            matches_won_per_team_obj[match['winner']] = {};
        }
        if (match['winner'] != '' && matches_won_per_team_obj[match['winner']][match['season']] === undefined) {
            matches_won_per_team_obj[match['winner']][match['season']] = 0;
        }
        if (match['winner'] != '') {
            matches_won_per_team_obj[match['winner']][match['season']] += 1;
        }
    })
    const data = JSON.stringify(matches_won_per_team_obj);
    const addr = '../public/output/matches_won_per_team_per_year.json';
    write_file(addr, data);
}

function write_file(addr, data) {
    fs.writeFile(addr, data, err => {
        if (err)
            throw (err);
    })
}

// calling function

economical_bowler_2015();
extra_runs();
match_won_per_team_per_year();
match_played_per_year();
