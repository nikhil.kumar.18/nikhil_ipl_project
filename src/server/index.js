let csvToJson = require('convert-csv-to-json');

csvToJson.fieldDelimiter(',').getJsonFromCsv("../data/deliveries.csv");
let json = csvToJson.getJsonFromCsv("../data/deliveries.csv");
let deliveries_arr=[]
for(let i=0; i<json.length;i++){
    deliveries_arr.push(json[i]);
}

csvToJson.fieldDelimiter(',').getJsonFromCsv("../data/matches.csv");
let json_2 = csvToJson.getJsonFromCsv("../data/matches.csv");
let matches_arr=[]
for(let i=0; i<json_2.length;i++){
    matches_arr.push(json_2[i]);
}

module.exports = {matches_arr,deliveries_arr};

