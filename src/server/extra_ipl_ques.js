const { matches_arr, deliveries_arr } = require('./index');
const fs = require('fs');
const { parse } = require('path');
function won_toss_won_match() {
    let won_toss_match = {};
    matches_arr.map(team => {
        if (team['toss_winner'] === team['winner']) {
            if (won_toss_match[team['winner']] === undefined) {
                won_toss_match[team['winner']] = 0;
            }
            won_toss_match[team['winner']] += 1;

        }
    });
    const addr = "../public/output/won_toss_match.json";
    write_file(addr, JSON.stringify(won_toss_match));
}

function player_of_match_season() {
    let player_of_match_per_season = {};
    matches_arr.map(match => {
        if (player_of_match_per_season[match['season']] === undefined)
            player_of_match_per_season[match['season']] = {};
        if (player_of_match_per_season[match['season']][match['player_of_match']] === undefined) {
            player_of_match_per_season[match['season']][match['player_of_match']] = 0;
        }

        player_of_match_per_season[match['season']][match['player_of_match']] += 1;
    })

    let max_no_of_player_of_match = {};
    for (let season in player_of_match_per_season) {

        let player_name = "";
        let no_of_time_won = 0;
        max_no_of_player_of_match[season] = {};
        let match = player_of_match_per_season;
        for (let player in match[season]) {

            if (no_of_time_won < match[season][player]) {
                no_of_time_won = match[season][player];
                player_name = player;
            }
        }

        max_no_of_player_of_match[season][player_name] = no_of_time_won;
    }

    const addr = "../public/output/player_of_match_each_season.json";
    write_file(addr, JSON.stringify(max_no_of_player_of_match));
}

function strike_rate() {
    let strike_rate_batsman = {};
    deliveries_arr.map(delivery => {
        matches_arr.map(match => {
            if (delivery['match_id'] === match['id']) {
                if (strike_rate_batsman[delivery['batsman']] === undefined) {
                    strike_rate_batsman[delivery['batsman']] = {};

                }
                if (strike_rate_batsman[delivery['batsman']][match['season']] === undefined) {
                    strike_rate_batsman[delivery['batsman']][match['season']] = { 'runs_scored': 0, 'balls_played': 0, 'strike_rate': 0 };
                }

                strike_rate_batsman[delivery['batsman']][match['season']]['runs_scored'] += parseInt(delivery['total_runs']);
                if (delivery['wide_runs'] == '0' && delivery['noball_runs'] == '0') {
                    strike_rate_batsman[delivery['batsman']][match['season']]['balls_played'] += 1;
                }

                strike_rate_batsman[delivery['batsman']][match['season']]['strike_rate'] = Math.round(((strike_rate_batsman[delivery['batsman']][match['season']]['runs_scored'] / strike_rate_batsman[delivery['batsman']][match['season']]['balls_played']) * 100) * 100) / 100;

            }
        });
    });
    const addr = "../public/output/strike_rate.json";
    write_file(addr, JSON.stringify(strike_rate_batsman));

}

function dismissed() {
    let dismissed = {};
    deliveries_arr.map(delivery => {
        if (delivery['player_dismissed']) {
            if (dismissed[delivery['bowler']] === undefined) {
                dismissed[delivery['bowler']] = {};
            }
            if (dismissed[delivery['bowler']][delivery['batsman']] === undefined) {
                dismissed[delivery['bowler']][delivery['batsman']] = 0;
            }
            dismissed[delivery['bowler']][delivery['batsman']] += 1;
        }
    })
    let player_out = "";
    let bowler = "";
    let no_of_times = 0;
    Object.keys(dismissed).map(bowlers => {
        Object.keys(dismissed[bowlers]).map(bowl => {
            if (dismissed[bowlers][bowl] > no_of_times) {
                no_of_times = dismissed[bowlers][bowl];
                bowler = bowlers;
                player_out = bowl;
            }
        })
    })
    const addr = "../public/output/dismissed.json";
    write_file(addr, JSON.stringify([bowler, player_out, no_of_times]));

}

function super_over() {
    let economy_rate_super_over = {};
    deliveries_arr.map(delivery => {
        if (delivery['is_super_over'] == '1') {
            if (economy_rate_super_over[delivery['bowler']] === undefined) {
                economy_rate_super_over[delivery['bowler']] = { 'runs_given': 0, 'no_of_balls': 0 };
            }
            if (delivery['wide_runs'] == '0' && delivery['noball_runs'] == '0') {
                economy_rate_super_over[delivery['bowler']]['no_of_balls'] += 1;
            }
            economy_rate_super_over[delivery['bowler']]['runs_given'] += parseInt(delivery['total_runs']);
        }
    })
    let best_bowler = { 'bowler': "", "economy_rate": 1000 }
    Object.keys(economy_rate_super_over).map(bowlers => {

        let eco_rate = (economy_rate_super_over[bowlers]['runs_given']) / parseFloat((economy_rate_super_over[bowlers]['no_of_balls']) / 6);

        if (eco_rate < best_bowler['economy_rate']) {
            best_bowler['economy_rate'] = eco_rate;
            best_bowler['bowler'] = bowlers;
        }
    })
    const addr = "../public/output/super_over_best_bowler.json";
    write_file(addr, JSON.stringify(best_bowler));
}



function write_file(addr, data) {
    fs.writeFile(addr, data, err => {
        if (err)
            throw (err);
    })
}

//calling funcitons
won_toss_won_match();
player_of_match_season();
strike_rate();
dismissed();
super_over();
