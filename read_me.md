### Ipl Data Project

This project uses data taken from ipl and finds out answers to these questions :- 


    Number of matches played per year for all the years in IPL.
    Number of matches won per team per year in IPL.
    Extra runs conceded per team in the year 2016
    Top 10 economical bowlers in the year 2015


#### Dependencies used :-

[http-server](https://www.npmjs.com/package/http-server)

[convert-csv-to-json](https://www.npmjs.com/package/convert-csv-to-json)

#### Steps to view data from project :- 

step 1. run npm instatll to install all dependencies.

step 2. Run ipl.js and extra_ipl_ques.js as this will dump all data into json files in output folder

step 3. type the command **npm start** (this will start http-server at ./src/public if server didn't start at /public plesase add **/public** in the url to run the project) 

Link to IPL data is [here.](https://www.kaggle.com/manasgarg/ipl)
